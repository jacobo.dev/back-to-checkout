class Line {
    constructor(item) {
        this.item = item
        this.quantity = 1
        this.totalPrice = 0
    }

    getItem() {
        return this.item
    }

    getQuantity() {
        return this.quantity
    }

    getTotalPrice() {
        return this.totalPrice
    }

    increaseQuantity() {
        this.quantity += 1
    }

    setPrice(price) {
        this.totalPrice = price
    }
}