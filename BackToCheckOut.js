/**********  POSITIONS **********/
const ITEM = 0

// ITEM RULE
const UNIT_PRICE = 1
const OFFER_QUANTITY = 2
const OFFER_PRICE = 3

class CheckOut {
    constructor(pricing_rules) {            //        0      1            2                 3
        this.pricing_rules = pricing_rules  // rule: [ITEM,  UNIT PRICE,  OFFER QUANTITY,   OFFER PRICE]
        this.ticket = []                    // row:  [ITEM,  QUANTITY,    PRICE]
        this.totalPrice = 0
    }

    price(str) {
        this.clearTicket()
        this.scanEveryItem(str)
        return this.totalPrice
    }

    clearTicket() {
        this.ticket = []
        this.totalPrice = 0
    }

    scanEveryItem(str) {
        let item = str.length
        while (item--) this.scan(str.charAt( item ))
    }

    scan(item) {
        this.addItemToTicket(item)
        this.updateSimpleOffer(item)
        this.updateTotalPrice()
    }

    addItemToTicket(item) {
        const index = this.ticket.findIndex( line => line.getItem() === item )

        if (index === -1) {
            const line = new Line(item)
            this.ticket.push(line)
        } 
        else this.ticket[index].increaseQuantity()
    }
    
    updateSimpleOffer(item) {
        const simpleRule = this.pricing_rules.find( rule => rule[ITEM] === item )
        const line = this.ticket.find( line => line.getItem() === item )
        const lineIndex = this.ticket.findIndex( line => line.getItem() === item )
        
        const linePrice = this.calculateLinePrice(simpleRule, line)
        this.ticket[lineIndex].setPrice(linePrice)
    }

    updateTotalPrice() {
        let sum = 0
        this.ticket.forEach( line => sum += line.getTotalPrice() )
        this.totalPrice = sum
    }

    calculateLinePrice(simpleRule, line) {
        const unitPrice = simpleRule[UNIT_PRICE]
        const offerQuantity = simpleRule[OFFER_QUANTITY]
        const quantityPurchased = line.getQuantity()

        if (this.hasAnOffer(offerQuantity, quantityPurchased)) 
            line.setPrice( this.getSimpleOfferTotal(simpleRule, line) )
        else
            line.setPrice( quantityPurchased * unitPrice )

        return line.getTotalPrice()
    }

    hasAnOffer(offerQuantity, quantityPurchased) {
        return offerQuantity !== null && quantityPurchased > 1
    }

    getSimpleOfferTotal(simpleRule, line) {
        const unitPrice = simpleRule[UNIT_PRICE]
        const offerQuantity = simpleRule[OFFER_QUANTITY]
        const offerPrice = simpleRule[OFFER_PRICE]
        const quantityPurchased = line.getQuantity()

        const numberOfOffers = ~~( quantityPurchased / offerQuantity )
        const unitsWithoutOffer = quantityPurchased % offerQuantity

        return numberOfOffers * offerPrice  +  unitsWithoutOffer * unitPrice
    }
}