describe("BACK TO CHECKOUT:", () => {
})

describe("Test totals", () => {
    // Arrange
    const PRICING_RULES_1 = [
        // [ITEM, UNIT PRICE, OFFER QUANTITY, OFFER PRICE]
        ["A", 50, 3, 130],
        ["B", 30, 2, 45],
        ["C", 20, null, null],
        ["D", 15, null, null]
    ]

    // Act
    co = new CheckOut(PRICING_RULES_1)

    // Assert
    it("Void ticket", () => expect(co.price("")).toBe(0))
    it("One item", () => expect(co.price("A")).toBe(50))
    it("Two different item", () => expect(co.price("AB")).toBe(80))
    it("Four different item", () => expect(co.price("CDBA")).toBe(115))

    it("Same item without offer", () => expect(co.price("AA")).toBe(100))
    it("Same item with 1 offer and NO remainder", () => expect(co.price("AAA")).toBe(130))
    it("Same item with 1 offer and 1 remainder", () => expect(co.price("AAAA")).toBe(180))
    it("Same item with 1 offer and 2 remainder", () => expect(co.price("AAAAA")).toBe(230))
    it("Same item with 2 offer and NO remainder", () => expect(co.price("AAAAAA")).toBe(260))
    it("Same item with NO AVAILABLE offer", () => expect(co.price("DD")).toBe(30))

    it("Several different items, 1 offer, 1 remainder", () => expect(co.price("AAAB")).toBe(160))
    it("Several different items, 2 offer, NO remainder", () => expect(co.price("AAABB")).toBe(175))
    it("Several different items, 2 offer, 1 remainder", () => expect(co.price("AAABBD")).toBe(190))
    it("Several different items, 2 offer, 1 remainder, mixed", () => expect(co.price("DABABA")).toBe(190))
})



describe("Test incremental", () => {
    const PRICING_RULES_1 = [
        ["A", 50, 3, 130],
        ["B", 30, 2, 45],
        ["C", 20, null, null],
        ["D", 15, null, null]
    ]
    
    co = new CheckOut(PRICING_RULES_1)

    it("No item scanned", () => expect(co.totalPrice).toBe(0))
    
    it("Add one item A", () => {
        co.scan("A")
        expect(co.totalPrice).toBe(50)
    })
    
    it("Add one item B", () => {
        co.scan("B")
        expect(co.totalPrice).toBe(80)
    })

    it("Add second item A (no offer)", () => {
        co.scan("A")
        expect(co.totalPrice).toBe(130)
    })

    it("Add third item A (with offer)", () => {
        co.scan("A")
        expect(co.totalPrice).toBe(160)
    })

    it("Add second item B (with offer)", () => {
        co.scan("B")
        expect(co.totalPrice).toBe(175)
    })
})